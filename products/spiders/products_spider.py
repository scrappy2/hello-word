from builtins import int
from scrapy.http import Response, Request
import scrapy

from products.items import ProductsItem


class ProductsSpiderSpider(scrapy.Spider):
    name = 'products-spider'
    allowed_domains = ['www.hubside.store']
    start_urls = ['http://www.hubside.store/fr/smartphones/apple']

    #custom_settings = {
    #    'ITEM_PIPELINES': {
    #        'app.MyPipeline': 400
    #    }
    #}

    def parse(self, response):

        product_names = response.xpath(
            ".//ol[@class='products list items product-items']//strong[contains(@class, 'product-item-name')]/a/text()").getall()
        product_prices = response.xpath(
            ".//ol[@class='products list items product-items']//div["
            "@class='product-price-wrap']//span/@data-price-amount").getall()

        for name, price in zip(product_names, product_prices):
            product = ProductsItem()
            product['name'] = name
            product['price'] = price

            yield product

        next_page = response.xpath(".//a[@class='action  next enabled']/@href").get()

        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)