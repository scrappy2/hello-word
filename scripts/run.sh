#!/bin/bash
function execute_spiders() {
  while read -r line; do
    echo "************************ Execute Spider $line ***********************"
    scrapy crawl -s MONGODB_URI="${MONGODB_URI}" -s MONGODB_DATABASE="${MONGODB_DATABASE}" "$line"
  done < spiders.txt
}

while true; do
  execute_spiders
  sleep 10
done