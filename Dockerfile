FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
RUN chmod u+x scripts/run.sh

ENTRYPOINT ["scripts/run.sh"]